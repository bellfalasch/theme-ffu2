<?php
/*
	Template Name: Guide (med guidenavigator)
*/
?><?php get_header(); ?>
<?php get_sidebar(); ?>

	<article>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<h1><?php the_title(); ?></h1>

			<?php get_guide_menu( $post->post_parent, $post->ID ); ?>

			<?php
				// Allow for page stumps to automatically output info how to apply for a job =)
				if ( mb_strlen(get_the_content()) < 4) {
					emptyPagePlaceholder();
				} else {
					the_content();
				}
			?>

			<?php get_guide_menu( $post->post_parent, $post->ID, true, true ); /* true = add rel=nofollow to links */ ?>

			<?php

				global $PAGE; // Needed for the footer.php to being able to fetch dates and author info from current page!
				$PAGE = $post;

			?>

		<?php endwhile; endif; ?>

	</article>

<?php get_footer(); ?>
