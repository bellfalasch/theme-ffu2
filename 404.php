<?php get_header(); ?>
<?php get_sidebar(); ?>

	<article>

		<h1>Vi finner inte sidan, kupo!</h1>
		<p style="margin-bottom:0; padding-bottom:14px; font-size:8pt; color:gray; font-style:italic;">Status: <strong>404</strong></p>

		<div class="fullcover">
			<img src="<?= get_template_directory_uri() ?>/assets/_design/404_magicurn.png" alt="Pixelbild på Magic Urn/Pot, från Final Fantasy-spelen" style="float:right; margin: 0 0 6px 6px;" />
			<p>
			  Tyvärr kan inte sidan du sökte efter hittas hos oss längre.
			  Kanske den flyttats?
			  Kanske den aldrig fanns?!
			  Kanske en hungrig Moogle åt upp den?
			  Kanske Magic Urn har tagit den!?
			</p>
			<p>
			  Vad som än hänt den så är den inte här, kupo!
			</p>
		</div>

		<p style="font-weight:bold; text-align: initial;">Men pröva gärna att ... </p>
		<ul>
			<li>Söka efter sidan i guiden:
				<form action="<?php echo get_option('home'); ?>/" method="get" class="search-form">
					<label for="s1" class="screen-reader-text"></label>
					<input type="text" id="s1" name="s" class="search-form-input" placeholder="Vad letade du efter?" />
					<input type="submit" value="Sök" class="search-form-submit" />
				</form>
			</li>
			<li>Gå tillbaka till <a href="#" onclick="window.history.back();">sidan du kom ifrån</a> och försök med en annan länk.</li>
			<li>Dubbelkolla så att adressen verkligen är rättstavad.</li>
			<li>Använd guidens meny för att hitta sidan.</li>
			<li>Pröva att gå <a href="<?= get_option('home'); ?>/">tillbaks till startsidan</a></li>
			<li>Spela lite Final Fantasy istället och försök igen lite senare.</li>
		</ul>
<?php /*
		<p style="
		    margin-left: -155px;
		    margin-right: -155px;
		    margin-top: 20px;
		    background-color: white;
		    padding: 16px 155px;
		    color: #666;
		    text-align: initial;
		    line-height: 160%;
		">
		  <strong>Detaljerad information:</strong><br />
		  <span class="weak" style="color:#999">
		  <asp:Label id="lblErrorMsg" runat="server" /><br />
		  <strong><asp:Label id="lblReferer" runat="server" /></strong><br />
		  <%= DateTime.Now %>
		  </span>
		</p>
*/ ?>

<?php /*
		<p style="text-align: center;">
		  För att rapportera felet, så att vi kan laga det, ska du kontakta
		  <<<lblMailtoLink>>>.
		  I det mailet ska du inkludera hela den detaljerade informationen ovan.
		</p>
*/ ?>


<?php /*
		<h1>Sidan fanns inte (kod 404)</h1>

		<div class="sidebar">
			<h4>Sök</h4>
			<p>Sök här ovanför, i sökrutan hos Tonberry!</p>
			<p>Bara skriv det du letar efter och tryck på Enter-tangenten på ditt skrivbord.</p>
		</div>

		<p>
			Kanske följde du en gammal länk eller bara skrev fel adress?
		</p>
		<p>
			Pröva att <strong>söka efter sidan</strong> du försökte nå, eller gå
			<a href="<?= get_option('home'); ?>/">tillbaks till startsidan</a>, eller använd menyn för att
			leta dig fram.
		</p>
*/ ?>
	</article>

<?php get_footer(); ?>
