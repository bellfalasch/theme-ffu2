<?php get_header(); ?>
<?php get_sidebar(); ?>

	<article>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<h1 style="font-size:18pt;"><?php the_title(); ?></h1>

			<?php
				$imageCode = wp_get_attachment_image( get_the_ID(), array('700', '600'), false, '' );
				echo $imageCode;

				// Needed for the footer.php to being able to fetch dates and author info from current page!
				global $PAGE;
				$PAGE = $post;
			?>

		<?php endwhile; endif; ?>

	</article>

<?php get_footer(); ?>
