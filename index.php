<?php get_header(); ?>
<?php get_sidebar(); ?>
<?php
	global $foldername_clean;
?>

	<div class="page_wrap">
		<section class="story">
<?php
	$args = array(
		'posts_per_page'   => 1,
		'category_name'    => 'Story'
	);

	$posts = get_posts( $args );

	if ($posts) {

		foreach($posts as $post) {

?>
			<h1><?= $post->post_title; ?></h1>

			<?= apply_filters('the_content', $post->post_content); ?>

<?php

		}

	} else {
		// Default text
		echo "<h1>";
		echo "Story saknas!";
		echo "</h1>";
		echo "<p>TODO: Skapa en ny \"Post\" inne i WordPress innhållandes några rader story/introduktion till spelet (se de andra spelsajterna). Lägg denna nya Post i kategorin \"Story\". Spara - färdigt (denna text försvinner).</p>";
	}

?>

		</section>

	<div class="sidebar_wrap">
		<aside>
<?php

	$args = array(
		'posts_per_page'   => 1,
		'category_name'    => 'Disclaimer'
	);

	$posts = get_posts( $args );

	if ($posts) {

		foreach($posts as $post) {

			// Suddenly WP stopped wrapping single paragraphs (like news) in p-tags, so do it by force
			$content = apply_filters('the_content', $post->post_content );
?>
			<h4><?= $post->post_title; ?></h4>
			<?= $content ?>

<?php

		}

	} else {
		// Default text
		echo "<h4>Fotnot saknas!</h4>";
		echo "<p>TODO: Skapa en ny \"Post\" inne i WordPress innhållandes några rader om till vilken version/konsol denna sajt är skriven för, se de andra spelsajterna för bra exempel du kan återanvända. Lägg denna nya Post i kategorin \"Story\". Spara - färdigt (denna text försvinner).</p>";
	}

?>
		</aside>

		<div class="social_buttons">
			<a href="https://discord.gg/ncGqDWC" class="discord">Chatta på <strong>Discord</strong></a>
			<a href="https://www.facebook.com/finalfantasy.nu/" class="facebook">Gilla oss på <strong>Facebook</strong></a>
			<a href="https://twitter.com/FFUniverse_nu" class="twitter" title="Följ oss på X (tidigare Twitter).">Följ oss på <strong>X</strong></a>
			<a href="https://instagram.com/FFUniverse" class="instagram">Hjärta oss på <strong>Instagram</strong></a>
			<a href="https://www.youtube.com/channel/UCv1qhT8C6LnQ5hpC-YDa75A" class="youtube">Se oss på <strong>Yotube</strong></a>
			<a href="https://open.spotify.com/user/bellfalasch/playlist/7Mz2lVCJg3O5BW9bfo0JDs" class="spotify">Final Fantasy på <strong>Spotify</strong></a>
			<a href="https://om.ffuniverse.nu/rss-feeds/" class="rss">Uppdateringar på <strong>RSS</strong></a>
		</div>

	</div>

	<section class="updates">

<?php

	$args = array(
		'posts_per_page'   => 5,
		'category_name'    => 'Uppdatering'
	);

	$posts = get_posts( $args );

	if ($posts) {

		foreach($posts as $post) {

			$content = apply_filters('the_content', $post->post_content );

?>

	<article>

		<div class="datebox">
			<span class="date_day"><?php the_time('d') ?></span>
			<span class="date_month"><?= substr( strDateToSwedish( "M",  strtotime( get_the_time(DATE_ATOM) ) ), 0, 3 ) ?></span>
			<span class="date_time"><?php the_time('Y') ?></span>
			<!--
			<span class="date_time"><?php the_time('H:i') ?></span>
			<span class="date_year"><?php the_time('Y') ?></span>
			-->
		</div>

		<h3><a href="<?= get_page_link($post->ID); ?>"><?= $post->post_title; ?></a></h3>
		<?= $content; ?>

	</article>

<?php

		}

		echo "</section>";
		echo "<p>";
		echo "<a href=\"https://guide.ffuniverse.nu/sista-andringarna/?guide=" . $foldername_clean  . "\" class=\"blockLinkButton\">Se fler uppdateringar &raquo;</a>";
		echo "<a href=\"https://guide.ffuniverse.nu/" . $foldername_clean  . "/feed/\" class=\"blockRSSButton\">Få detta som RSS-feed istället</a>";
		echo "</p>";

	} else {
		// Default text
		echo "<article><h3>Uppdateringar saknas!</h3>";
		echo "<p>TODO: Skapa en ny \"Post\" inne i WordPress innhållandes några rader om vad som är nytt och uppdaterat sedan sist. Lägg denna nya Post i kategorin \"Uppdatering\". Spara - färdigt (denna text försvinner).</p></article>";
		echo "</section>";
	}

?>

	</div>

<?php get_footer(); ?>
