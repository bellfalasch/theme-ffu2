<?php

	// Include files
	////////////////////////////////////////////////////////////////////////
	require_once( get_template_directory() . '/inc/util-functions.php' );
	require_once( get_template_directory() . '/inc/theme-layout.php' );
	require_once( get_template_directory() . '/inc/ffu-updates.php' );
	require_once( get_template_directory() . '/inc/tinymce-extensions.php' );
	require_once( get_template_directory() . '/inc/photo-gallery.php' );

	/**
	 * Set the content width based on the theme's design and stylesheet.
	 *
	 * Used to set the width of images and content. Should be equal to the width the theme
	 * is designed for, generally via the style.css stylesheet.
	 */
	if ( !isset($content_width) ) {
		$content_width = 640;
	}

	////////////////////////////////////////////////////////////////////////
	// Theme support and image settings
	////////////////////////////////////////////////////////////////////////

	// This theme uses Featured Images
	add_theme_support( 'post-thumbnails' );

	// The height and width of your custom header.
	// Add a filter to twentyeleven_header_image_width and twentyeleven_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'twentyeleven_header_image_width', 960 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'twentyeleven_header_image_height', 250 ) );

	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be the size of the header image that we just defined
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Add Twenty Eleven's custom image sizes
	add_image_size( 'large-feature', HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true ); // Used for large feature (header) images
	add_image_size( 'small-feature', 500, 300 ); // Used for featured posts if a large-feature doesn't exist

	// Attach our custom editor stylesheet to the TinyMCE editor in Admin
	////////////////////////////////////////////////////////////////////////
	add_editor_style('editor-style.css');

	// Filter that can detect links to images and wrap them with correct lightcase syntax.
	// https://www.isitwp.com/add-rel-lightbox-to-all-images-embedded-in-a-post/
	////////////////////////////////////////////////////////////////////////
	function my_addlightcase($content) {
		global $post;
		$pattern ="/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
		$replacement = '<a$1href=$2$3.$4$5 data-rel="lightcase"$6>';
		$content = preg_replace($pattern, $replacement, $content);
		return $content;
	}
	add_filter('the_content', 'my_addlightcase');


	// JavaScript handling (correct enqueuing)
	////////////////////////////////////////////////////////////////////////
	function load_javascript_files() {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js');
		wp_enqueue_script( 'jquery' );

		if ( !is_admin() ) {
			wp_register_script( 'globals', get_template_directory_uri() . '/js/global.js', array('jquery'), '1.0', true );
			wp_enqueue_script( 'globals' );
		}
	}
	add_action( 'wp_enqueue_scripts', 'load_javascript_files' );

	// Disable sidebar and widgets from admin (theme doesn't use them)
	////////////////////////////////////////////////////////////////////////
	add_filter( 'sidebars_widgets', 'disable_all_widgets' );
	function disable_all_widgets( $sidebars_widgets ) {
		if ( is_home() )
			$sidebars_widgets = array( false );
		return $sidebars_widgets;
	}

	// We don't need to bloat the Admin with menues we never use
	////////////////////////////////////////////////////////////////////////
	function remove_menus() {
		global $menu;
		//$restricted = array(__('Tools'),__('Comments'));
		$restricted = array(__('Comments'));
		end ($menu);
		while (prev($menu)){
			$value = explode(' ',$menu[key($menu)][0]);
			if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
		}
	}
	add_action('admin_menu', 'remove_menus');

	// Disable Gutenberg editor.
	////////////////////////////////////////////////////////////////////////
	add_filter('use_block_editor_for_post_type', '__return_false', 10);
	// Don't load Gutenberg-related stylesheets.
	add_action( 'wp_enqueue_scripts', 'remove_block_css', 100 );
	function remove_block_css() {
	    wp_dequeue_style( 'wp-block-library' ); // Wordpress core
	    wp_dequeue_style( 'wp-block-library-theme' ); // Wordpress core
	    wp_dequeue_style( 'wc-block-style' ); // WooCommerce
	    wp_dequeue_style( 'storefront-gutenberg-blocks' ); // Storefront theme
	}

	remove_shortcode('gallery');
	add_shortcode('gallery', 'ffu_photo_gallery_generator');
?>
