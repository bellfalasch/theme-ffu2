<?php
/*
	Template Name: Vanlig sida (meny med ankare)
*/
?><?php get_header(); ?>
<?php get_sidebar(); ?>

	<article>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<h1><?php the_title(); ?></h1>

			<?php
				// If Thumbnail ("Featured Image") is added, display it
				if ( has_post_thumbnail() ) {
					echo "<div class=\"alignright\">" . get_the_post_thumbnail($id, 'medium') . "</div>";
				}
			?>

			<?php

				$content = apply_filters( 'the_content', get_the_content() );
				$content = str_replace(']]>', ']]&gt;', $content);
				$content = str_replace('–', '&#8211;', $content);

				//echo esc_html($content);

				// http://stackoverflow.com/questions/10524838/get-value-of-h2-of-html-page-with-php-dom
				// Find all h2 on the page, wrap them and their following text, store each h2 in a array and create a menu out of it on the top

				$linkhtml = mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8');

				$doc = new DOMDocument();
				libxml_use_internal_errors(true);
				$doc->loadHTML($linkhtml); // loads your html
				$xpath = new DOMXPath($doc);
				$elements = $xpath->query("//h2");
				$page_menus = array();

				if (!is_null($elements)) {
					$i = 0;
					foreach ($elements as $element) {
						$nodes = $element->childNodes;
						$nodevalue = $nodes->nodeValue;
						//var_dump($nodevalue);

						$wrapper = "<!-- start filter-wrapper #" . ($i + 1) . " --><div class=\"filter-wrapper\"";
						if ( $i > 0 ) {
							$wrapper = "</div><!-- end filter-wrapper #" . ($i) . " -->" . $wrapper;
						} else {
							// Found our first h2, add the menu html.
							$wrapper = "
								<nav role=\"navigation\" class=\"submenu simple floating\">
									<p class=\"heading\">Gå direkt till:</p>
									<ol>
										[[{{INSERT_MENU}}]]
									</ol>
								</nav>
								" . $wrapper;
						}
						$i++;

						foreach ($nodes as $node) {
							//echo $node->nodeValue. "<br />\n";
							$nodevalue = $node->nodeValue;
							$headline = $nodevalue;
							$old_headline = "<h2>" . $headline . "</h2>";
							/*
							$headline = str_replace('“', '&#8220;', $headline);
							$headline = str_replace('”', '&#8221;', $headline);
							$headline = str_replace('<', '&lt;', $headline);
							$headline = str_replace('>', '&gt;', $headline);
							$headline = str_replace('–', '&#8211;', $headline);
							*/
							$new_headline = "<h2>" . esc_html($headline) . "</h2>";
//							echo $headline . "<br />\n";
//							echo esc_html($headline) . "<br />\n";

							$wrapper = $wrapper . " id=\"" . titleToUrl($headline) . "\">"; // Add the anchor id and close html tag
							$content = str_replace($old_headline, $wrapper . $new_headline, $content); // Insert into to the content
							$page_menus[] = $headline;
						}
					}
				}
				$content = $content . "</div><!-- last filter-wrapper -->";

				$page_menu_html = "";
				foreach ($page_menus as $menu) {
					$url = titleToUrl($menu);
					$page_menu_html .= "<li>
						<a href=\"#" . $url . "\">" . $menu . "</a>
					</li>";
				}
				$content = str_replace( "[[{{INSERT_MENU}}]]", $page_menu_html, $content);

				echo $content;

			?>

			<?php
				global $PAGE; // Needed for the footer.php to being able to fetch dates and author info from current page!
				$PAGE = $post;
			?>
		<?php endwhile; endif; ?>

	</article>

<?php get_footer(); ?>
