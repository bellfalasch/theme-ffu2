<!-- sidebar -->
			<div id="navbar">
				<button class="menu-toggler toggle-open" aria-hidden="true">&Ouml;ppna menyn</button>
				<button class="menu-toggler toggle-close" aria-hidden="true">St&auml;ng menyn</button>
				
				<form action="<?php echo get_option('home'); ?>/" class="mobile-search-form" method="get" aria-hidden="true">
					<label for="s" class="screen-reader-text"></label>
					<input type="text" class="input-text" name="s" value="" placeholder="Vad letar du efter?" />
					<input type="submit" value="Sök" class="input-submit" />
				</form>

				<div id="logo">
					<a href="<?= get_option('home') ?>/">
						<?php
							$foldername = site_url();
							$foldername = str_replace( network_site_url(), '', $foldername );
						?>
						<img src="<?php bloginfo("template_url"); ?>/assets/logos/<?= $foldername ?>.png" alt="<?php bloginfo('name'); ?> logo" title="<?php bloginfo('name'); ?> logo" />
					</a>
				</div>

				<nav role="navigation" aria-expanded="false">

<?php

	/* *******************
	 * BEGIN MENU
	 ********************* */
	require_once( get_template_directory() . '/inc/menu.php' );
	global $post;
	global $menuHtml;
	$menuHtml = getMenu($post);
	echo $menuHtml;

?>

				</nav>

			</div>

			<div id="content">

				<nav role="navigation">
					<ol id="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
						<li itemprop="itemListElement" itemscope
					      itemtype="https://schema.org/ListItem"><a href="https://guide.ffuniverse.nu/" title="Se alla våra spelguider" itemprop="item"><span itemprop="name">FFU</span></a><meta itemprop="position" content="1" /></li>
						<?= get_breadcrumb(); ?>
					</ol>
				</nav>
<!-- /sidebar -->
