<?php
/*
	Template Name: En Boss
*/
?><?php get_header(); ?>
<?php get_sidebar(); ?>

	<article>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<h1><?php the_title(); ?></h1>
			<?php
				// Bossen har ett fullt namn (längre än det som står i striden).
				if ( get_field('fullt_namn') != "" ) {
					echo "<h2 class=\"fullname\">";
					//echo "<strong>Fullt namn:</strong> " . get_field('fullt_namn') . "<br />";
					echo get_field('fullt_namn');
					echo "</h2>";
				}

				// Bossen är också väldigt känd under ett annat namn (kanske genom olika översättningar)
				if ( get_field('ocksa_kand_som') != "" ) {
					echo "<h2 class=\"aka\">";
					echo get_field('ocksa_kand_som');
					echo "</h2>";
				}


				echo "<div class=\"infobox light\"><p>";

				$this_hp = get_field('hp');
				if ($this_hp == "" || $this_hp == "?") {
					$this_hp = "?";
				} else {
					$this_hp = number_format($this_hp, 0, ',', ' ');
				}

				$this_mp = get_field('mp');
				if ($this_mp == "")
					$this_mp = "?";

				$this_lvl = get_field('level');
				if ($this_lvl == "")
					$this_lvl = "?";

			?>

					<span<?php if ($this_hp === "?") echo ' class="unknown"'; ?>>
						<strong>HP:</strong> <?= $this_hp ?>
					</span>
					<span<?php if ($this_mp === "?") echo ' class="unknown"'; ?>>
						<strong>MP:</strong> <?= $this_mp ?>
					</span>
					<span<?php if ($this_lvl === "?") echo ' class="unknown"'; ?>>
						<strong>Lvl:</strong> <?= $this_lvl ?>
					</span>

			<?php

				echo "</p></div>";

			?>

			<div class="alignright">
				<?php

					$portratt = get_field('portratt');

					if ( !empty($portratt) ) {

						echo "<img src=\"" . $portratt["sizes"]["medium"] . "\" alt=\"\">";

					} else {

						echo get_the_post_thumbnail($id, 'medium'); // Eller: large

					}

				?>
			</div>

			<h2 class="boss-guide-intro">Så besegrar du <?php the_title(); ?></h2>

		<?php
		 //////////////////////////////////////////////////////////////////
		 // Other Bosses that are a part of this boss:
		 //////////////////////////////////////////////////////////////////
			/*
			*  Query posts for a relationship value.
			*  This method uses the meta_query LIKE to match the string "123" to the database value a:1:{i:0;s:3:"123";} (serialized array)
			*/
				$partBosses = get_posts(array(
					'post_type' => 'page',
					'post_status' => 'draft', // These part-bosses are always set as DRAFT since we don't want them inside menues etc.
					'meta_query' => array(
						array(
							'key' => 'partof', // name of custom field
							'value' => '"' . get_the_ID() . '"', // matches exactly "123", not just 123. This prevents a match for "1234"
							'compare' => 'LIKE'
						)
					)
				));

				?>

				<?php if( $partBosses ): ?>
					<aside class="highlight">
						<h4>Med i striden:</h4>

					<?php foreach( $partBosses as $boss ): ?>
						<p>
							<strong><?php echo get_the_title( $boss->ID ); ?></strong><br />

							<?php

								$this_hp = get_field('hp', $boss->ID);
								if ($this_hp == "")
									$this_hp = "?";

								$this_mp = get_field('mp', $boss->ID);
								if ($this_mp == "")
									$this_mp = "?";

								$this_lvl = get_field('level', $boss->ID);
								if ($this_lvl == "")
									$this_lvl = "?";

							?>

							<small>
								<?= $this_hp ?> HP -
								<?= $this_mp ?> MP -
								Lvl <?= $this_lvl ?><br />
							</small>

							<?php if (get_field('svagheter', $boss->ID) != "") { ?>
								Svag mot: <?= get_field('svagheter', $boss->ID) ?>.<br />
							<?php } ?>

							<?php if (get_field('stark_mot', $boss->ID) != "") { ?>
								Stark mot: <?= get_field('stark_mot', $boss->ID) ?>.<br />
							<?php } ?>
						</p>
					<?php endforeach; ?>

					</aside>

				<?php endif; ?>

	<?php
	 //////////////////////////////////////////////////////////////////
	 //////////////////////////////////////////////////////////////////
	?>

			<?php if (get_field('rekommenderad_lvl') != "") { ?>
				<div class="infobox short">
					<p>Vi rekommenderar <strong>level <?= get_field('rekommenderad_lvl') ?></strong> på ditt party för denna boss.</p>
				</div>
			<?php } ?>

			<?php if (get_field('exp') != "" || get_field('gil') != "" || get_field('svagheter') != "" || get_field('stark_mot') != "" || get_field('drop') != "" || get_field('steal') != "") { ?>

				<aside>
					<h4>Övrig information</h4>
					<?php
						$this_exp = get_field('exp');
						if ($this_exp != "") {
							$this_exp = number_format($this_exp, 0, ',', ' ');
						}

						$this_gil = get_field('gil');
						if ($this_gil != "") {
							$this_gil = number_format($this_gil, 0, ',', ' ');
						}

						if ( $this_exp != "" || $this_gil != "" ) {
					?>

					<p>
						<?php if ( $this_exp != "" ) { ?><strong>Exp:</strong> <?= $this_exp ?><br /><?php } ?>
						<?php if ( $this_gil != "" ) { ?><strong>Gil:</strong> <?= $this_gil ?><?php } ?>
					</p>

					<?php } ?>

					<?php if (get_field('svagheter') != "") { ?>
						<p><strong>Svag mot:</strong><br /><?= get_field('svagheter') ?></p>
					<?php } ?>

					<?php if (get_field('stark_mot') != "") { ?>
						<p><strong>Stark mot:</strong><br /><?= get_field('stark_mot') ?></p>
					<?php } ?>

					<?php if (get_field('drop') != "") { ?>
						<p><strong>Tappar:</strong><br /><?= get_field('drop') ?></p>
					<?php } ?>

					<?php if (get_field('steal') != "") { ?>
						<p><strong>Stjäl:</strong><br /><?= get_field('steal') ?></p>
					<?php } ?>

				</aside>

			<?php } ?>

			<?php the_content(); ?>

			<?php get_sibling_menu( $post->post_parent, $post->ID, "Boss", "bossar" ); ?>

			<?php
				global $PAGE; // Needed for the footer.php to being able to fetch dates and author info from current page!
				$PAGE = $post;
			?>
		<?php endwhile; endif; ?>

	</article>

<?php get_footer(); ?>
