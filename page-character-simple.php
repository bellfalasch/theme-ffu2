<?php
/*
	Template Name: En Karaktär - enkel
*/
?><?php get_header(); ?>
<?php get_sidebar(); ?>

	<article class="character">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php echoCharacterPortrait(get_field('portratt'),get_the_post_thumbnail($id, 'medium')); ?>

			<h1><?php the_title(); ?></h1>

			<?php

				if (get_field('tagline_/_motto') != "") {
					echo "<blockquote>" . get_field('tagline_/_motto') . "</blockquote>";
				}

			?>

			<?php echoCharacterNotPlayable(get_field('spelbar_karaktar'),get_field('kon')); ?>

			<?php the_content(); ?>

			<?php get_sibling_menu( $post->post_parent, $post->ID, "Karaktär", "karaktärer" ); ?>

			<?php
			global $PAGE; // Needed for the footer.php to being able to fetch dates and author info from current page!
			$PAGE = $post;
			?>
		<?php endwhile; endif; ?>

	</article>

<?php get_footer(); ?>
