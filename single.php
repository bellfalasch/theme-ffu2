<?php get_header(); ?>
<?php get_sidebar(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php
			// If Thumbnail ("Featured Image") is added, display it
			if ( has_post_thumbnail() ) {
				echo "<div class=\"alignright\">" . get_the_post_thumbnail($id, 'medium') . "</div>";
			}
		?>
		<section class="updates">
			<article>
				<div class="datebox">
					<span class="date_day"><?php the_time('d') ?></span>
					<span class="date_month"><?= substr( strDateToSwedish( "M",  strtotime( get_the_time(DATE_ATOM) ) ), 0, 3 ) ?></span>
					<span class="date_time"><?php the_time('Y') ?></span>
				</div>

				<h1><?php the_title(); ?></h1>
				
				<?php the_content(); ?>
			</article>
		</section>

		<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
