<?php get_header(); ?>
<?php get_sidebar(); ?>

	<article>

		<h1>Sökresultat</h1>
		<?php
			$searchData = new WP_Query("s=$s&showposts=-1");
			$keyword = wp_specialchars($s, 1);
			$count = $searchData->post_count;
			wp_reset_query();
		?>
		<?php if (have_posts()) : ?>
			<p>Du sökte efter "<strong><?= $keyword ?></strong>", vi hittade <strong><?= $count ?></strong> sidor (de som matchade bäst ligger överst). Bara klicka på rubriken för att öppna sidan, finner du inte det du söker så kom tillbaka hit och leta vidare.</p>

			<form action="<?php echo get_option('home'); ?>/" method="get" class="search-form">
				<label for="s1" class="screen-reader-text"></label>
				<input type="text" id="s1" name="s" value="<?= $s ?>" class="search-form-input" placeholder="Vad letar du efter?" />
				<input type="submit" value="Sök" class="search-form-submit" />
			</form>
			<hr />
			
			<?php while (have_posts()) : the_post(); ?>
				<div class="searchblock">
					<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a><br />
					<a href="<?php the_permalink(); ?>" class="url"><?php echo str_replace("https://guide.ffuniverse.nu","", str_replace("http://guide.ffuniverse.nu","",get_permalink()) ); ?></a>
					<?php the_excerpt(); ?>
				</div>
			<?php endwhile; ?>
			
			<hr />
			<p>Fick du inte det resultat du väntat dig? Sök på nytt:</p>
		<?php else : ?>
			<p>Tyvärr, din sökning efter "<strong><?= $keyword ?></strong>" hittade vi inget för. Pröva ändra lite på stavningen och sök igen.</p>
		<?php endif; ?>

		<form action="<?php echo get_option('home'); ?>/" method="get" class="search-form">
			<label for="s2" class="screen-reader-text"></label>
			<input type="text" id="s2" name="s" value="<?= $s ?>" class="search-form-input" placeholder="Vad letar du efter?" />
			<input type="submit" value="Sök" class="search-form-submit" />
		</form>

	</article>

<?php get_footer(); ?>
