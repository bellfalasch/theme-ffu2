<?php
/*
	Template Name: Bildgalleri - enkel
*/
?><?php get_header(); ?>
<?php get_sidebar(); ?>

	<article>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<h1><?php the_title(); ?></h1>

			<?php the_content(); ?>

<?php

	$attachments = get_children(
					  array('post_parent' => $post->ID,
							'post_status' => 'inherit',
							'post_type' => 'attachment',
							'post_mime_type' => 'image',
							'order' => 'DESC',
							'orderby' => 'menu_order ID')
					);


	echo "<ol class=\"image_gallery\">";

	foreach($attachments as $att_id => $attachment) {

		$full_img_url = wp_get_attachment_image_url($attachment->ID, 'large');
		$thumb_img_url = wp_get_attachment_image_url($attachment->ID, 'thumbnail'); // Thumbnail is also default size returned
		$split_pos = strpos($full_img_url, 'wp-content');
		$split_len = (strlen($full_img_url) - $split_pos);
		$abs_img_url = substr($full_img_url, $split_pos, $split_len);
		$full_info = @getimagesize(ABSPATH.$abs_img_url);

		if ($attachment->post_excerpt != '') {
			$img_title = $attachment->post_excerpt;
		} elseif ($attachment->post_title != '') {
			$img_title = $attachment->post_title;
		}

?>
		<li>
			<a href="<?= $full_img_url; ?>" title="<?= $img_title; ?>" data-rel="lightcase:group">
				<img src="<?= $thumb_img_url; ?>" alt="" />
			</a>
		</li>

<?php

	}

	echo "</ol>";

?>

			<?php
				global $PAGE; // Needed for the footer.php to being able to fetch dates and author info from current page!
				$PAGE = $post;
			?>

		<?php endwhile; endif; ?>

	</article>

<?php get_footer(); ?>
