<!DOCTYPE html>
<?php if (1 == 2) { ?><html <?php language_attributes(); ?>><?php } ?>
<html lang="sv">
<head>
	<meta charset="<?php bloginfo('charset'); ?>"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title><?php
		  if (function_exists('is_tag') && is_tag()) {
			 single_tag_title("Tagg-arkiv för &quot;"); echo '&quot; - '; }
		  elseif (is_archive()) {
			 wp_title(''); echo ', översikt - '; }
		  elseif (is_search()) {
			 echo 'Sökning efter &quot;'.esc_html($s).'&quot; - '; }

			// If sub page, get the parent page's name for the title (only works one level deep!)
		  elseif ( !(is_404()) && (is_single()) || (is_page()) ) {

				// This is a normal page with a parent:
				if ( is_page() && $post->post_parent ) {
			  		wp_title(''); echo ', ' . get_the_title( $post->post_parent ) . ' - ';
			  // A post, this gets complicated ...
			  } else {

			  		// If is_single it can be a post or a Custome Page Type, we then need to check between these two and fetch CPT-title
			  		$post_type = get_post_type_object( get_post_type($post) );

			  		// It's a CPT if the label is set, then output it - but not for the type Pages (Pages are actually a CPT, Posts declared as Pages)
			  		if ($post_type->label && $post_type->label != "Pages") {
							wp_title(''); echo ', ' . $post_type->label . ' - ';
			  		// A normal (parent) page/post meta title
			  		} else {
							wp_title(''); echo ' - ';
						}
				}

		  } elseif (is_404()) {
			 echo 'Inte funnet - '; }

		//$foldername = 'FF7';
		// To get the folder name (like "FF7") in the end of every title, find out the current folder for this installation.
		$foldername = site_url();
		$foldername = str_replace( network_site_url(), '', $foldername );
		//$foldername = str_replace( $foldername, '/', '' );
		global $foldername_clean;
		$foldername_clean = $foldername;

		$foldername = strtoupper($foldername);

		// If foldername is too long, we most likely have a full titled folder, like with "bravely-default"
		// "<= 6" will accept ff7, ff10, ffxiv and ffxiii
		if ( mb_strlen($foldername) <= 6 ) {
			$foldername = ' (' . $foldername . ')';
		} else {
			$foldername = '';
		}

		if ( is_home() ) {
			bloginfo('name'); echo $foldername . ' - '; bloginfo('description');
		} else {
			bloginfo('name'); echo $foldername;
		}

		if ( $paged > 1 ) {
			echo ' - sida '. $paged;
		}

	   ?></title>
	<?php if (is_search()) { ?><meta name="robots" content="noindex, nofollow" /><?php } ?>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type='text/css' />
	<link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/css/lightcase.css" type='text/css' />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name') ?> &raquo; Feed" href="<?= site_url(); ?>/feed/" />

	<link rel="icon" href="<?php bloginfo("template_url"); ?>/assets/favicon.png" sizes="32x32">
	<link rel="icon" href="<?php bloginfo("template_url"); ?>/assets/favicon.png" sizes="192x192">
	<link rel="apple-touch-icon" href="<?php bloginfo("template_url"); ?>/assets/favicon.png">
	<meta name="msapplication-TileImage" content="<?php bloginfo("template_url"); ?>/assets/favicon.png">

	<!--[if lt IE 9]><script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php do_action('body_opened'); ?>
	<?php do_action('ffu_topnav'); ?>

	<header class="theme-header">
		<div class="wrapper" style="background-image:url('<?php bloginfo("template_url"); ?>/assets/headers/<?= $foldername_clean ?>.jpg');">
			<div class="content">

				<p class="site_title"><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></p>

				<form action="<?php echo get_option('home'); ?>/" id="searchform" method="get">
					<div>
						<label for="s" class="screen-reader-text"></label>
						<input type="text" id="s" name="s" value="<?= $s ?>" placeholder="Sök:" />
						<input type="submit" value="" id="searchsubmit" title="Klicka på Tonberry för att söka!" />
					</div>
				</form>

			</div>
		</div>
	</header>

	<div id="main_wrapper">
		<div id="content_wrapper">
<!-- /header -->
