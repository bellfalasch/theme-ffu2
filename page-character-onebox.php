<?php
/*
	Template Name: En Karaktär - en faktaruta
*/
?><?php get_header(); ?>
<?php get_sidebar(); ?>

	<article class="character">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php echoCharacterPortrait(get_field('portratt'),get_the_post_thumbnail($id, 'medium')); ?>

			<aside>
				<h4>Om <?php the_title(); ?></h4>
				<?php

					$facts = 0;

					// Diverse info
					if (get_field('fullt_namn') != "") {
						echo "<p><strong>Fullt namn:</strong><br />" . get_field('fullt_namn') . "</p>";
						$facts++;
					}

					if (get_field('smeknamn') != "") {
						echo "<p><strong>Också känd som:</strong><br />" . get_field('smeknamn') . "</p>";
						$facts++;
					}

					if (get_field('amerikansk_rostskadespelare') != "") {
						echo "<p><strong>Amerikansk röst:</strong><br />" . get_field('amerikansk_rostskadespelare') . "</p>";
						$facts++;
					}

					if (get_field('japansk_rostskadespelare') != "") {
						echo "<p><strong>Japansk röst:</strong><br />" . get_field('japansk_rostskadespelare') . "</p>";
						$facts++;
					}

					if ( $facts === 0 ) {
						echo "<p>- Inga fakta än -</p>";
					}

					// Fakta
					echo "<h4>Snabbfakta</h4><p>";
					$facts = 0;

					if (get_field('kon') != "" && get_field('kon') != "Visa ej") {
						echo "<strong>Kön:</strong> " . get_field('kon') . "<br />";
						$facts++;
					}
					if (get_field('ålder') != "") {
						echo "<strong>Ålder:</strong> " . get_field('ålder') . " år<br />";
						$facts++;
					}
					if (get_field('ras') != "") {
						echo "<strong>Ras:</strong> " . get_field('ras') . "<br />";
						$facts++;
					}
					if (get_field('langd_i_cm') != "") {
						echo "<strong>Längd:</strong> " . get_field('langd_i_cm') . " cm<br />";
						$facts++;
					}
					if (get_field('vikt_i_kg') != "") {
						echo "<strong>Vikt:</strong> " . get_field('vikt_i_kg') . " kg<br />";
						$facts++;
					}
					if (get_field('hemstad') != "") {
						echo "<strong>Hemort:</strong> " . get_field('hemstad') . "<br />";
						$facts++;
					}
					if (get_field('yrke') != "") {
						echo "<strong>Yrke:</strong> " . get_field('yrke') . "<br />";
						$facts++;
					}
					if (get_field('fodelsedag') != "") {
						echo "<strong>Födelsedag:</strong> " . get_field('fodelsedag') . "<br />";
						$facts++;
					}
					if (get_field('vapen') != "") {
						echo "<strong>Vapen:</strong> " . get_field('vapen') . "";
						$facts++;
					}

					if ( $facts === 0 ) {
						echo "- Inga fakta än -";
					}
					echo "</p>";

				?>
			</aside>

			<h1><?php the_title(); ?></h1>

			<?php

				if (get_field('tagline_/_motto') != "") {
					echo "<blockquote>" . get_field('tagline_/_motto') . "</blockquote>";
				}

			?>

			<?php echoCharacterNotPlayable(get_field('spelbar_karaktar'),get_field('kon')); ?>

			<?php the_content(); ?>

			<?php get_sibling_menu( $post->post_parent, $post->ID, "Karaktär", "karaktärer" ); ?>

			<?php
				global $PAGE; // Needed for the footer.php to being able to fetch dates and author info from current page!
				$PAGE = $post;
			?>
		<?php endwhile; endif; ?>

	</article>

<?php get_footer(); ?>
