<?php
/*
	Template Name: Vanlig sida (filter/search)
*/
?><?php get_header(); ?>
<?php get_sidebar(); ?>

	<article>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<h1><?php the_title(); ?></h1>

			<?php

				// If Thumbnail ("Featured Image") is added, display it
				if ( has_post_thumbnail() ) {

					echo "<div class=\"alignright\">" . get_the_post_thumbnail($id, 'medium') . "</div>";

				}
			?>

			<?php

				$content = apply_filters( 'the_content', get_the_content() );
				$content = str_replace( ']]>', ']]&gt;', $content );
				$content = str_replace('–', '&#8211;', $content);

				// http://stackoverflow.com/questions/10524838/get-value-of-h2-of-html-page-with-php-dom
				// Find all h2 on the page, wrap them and their following text

				$linkhtml = $content;

				$doc = new DOMDocument();
				libxml_use_internal_errors(true);
				$doc->loadHTML(mb_convert_encoding($linkhtml, 'HTML-ENTITIES', 'UTF-8')); // loads your html as UTF-8
				$xpath = new DOMXPath($doc);
				$elements = $xpath->query("//h2");

				if (!is_null($elements)) {
					$i = 0;
					foreach ($elements as $element) {
						$nodes = $element->childNodes;
//						$nodevalue = $nodes->nodeValue;
//						var_dump($nodevalue);

						$wrapper = "<!-- start filter-wrapper #" . ($i + 1) . " --><div class=\"filter-wrapper\"";
						if ( $i > 0 ) {
							$wrapper = "</div><!-- end filter-wrapper #" . ($i) . " -->" . $wrapper;
						} else {
							// First h2, add the form html
							$wrapper = "
								<div class=\"fullcover filter-box\">
									<h4>Snabbfiltrera!</h4>
									<p>Skriv något i fältet för att omedelbart filtrera fram det du söker efter på denna sida. Klicka på \"Rensa filtret\" för att visa allt igen.</p>
									<input type=\"text\" class=\"filter-text\" />
									<!--<input type=\"button\" class=\"filter-button\" value=\"Filtrera\" />-->
									<input type=\"button\" class=\"filter-clean\" value=\"Rensa filtret\" />
									<p><small>OBS! Söket görs endast på rubriker.</small></p>
								</div>
								<nav role=\"navigation\" class=\"submenu simple floating tiny\">
									<ol>
										[[{{INSERT_MENU}}]]
									</ol>
								</nav>
								" . $wrapper;
						}
						$i++;

						foreach ($nodes as $node) {
//							echo $node->nodeValue. "<br />\n";
							$nodevalue = $node->nodeValue;
							$headline = $nodevalue;
							// Begin replacing characters that might break our HTML
							$headline = str_replace('“', '&#8220;', $headline);
							$headline = str_replace('”', '&#8221;', $headline);
							$headline = str_replace('<', '&lt;', $headline);
							$headline = str_replace('>', '&gt;', $headline);
							$headline = str_replace("’", '&#8217;', $headline);
//							echo $headline . "<br />\n";
							$old_headline = "<h2>" . $headline . "</h2>";
							$new_headline = "<h2>" . esc_html($headline) . "</h2>";

							$wrapper = $wrapper . " id=\"" . titleToUrl($headline) . "\">"; // Add the anchor id and close html tag
							$content = str_replace($old_headline, $wrapper . $new_headline, $content); // Insert into to the content
							$page_menus[] = $nodevalue;
						}

					}
				}
				$content = $content . "</div><!-- last filter-wrapper -->";

				// <div class="form-well"><form method="get" action=""><input type="text" name="filter" class="filter-search" /><input type="submit" value="Filtrera" /></form></div>

				$page_menu_html = "";
				foreach ($page_menus as $menu) {
					$url = titleToUrl($menu);
					$page_menu_html .= "<li>
						<a href=\"#" . $url . "\">" . $menu . "</a>
					</li>";
				}
				$content = str_replace( "[[{{INSERT_MENU}}]]", $page_menu_html, $content);

				echo $content;

			?>

			<?php

				global $PAGE; // Needed for the footer.php to being able to fetch dates and author info from current page!
				$PAGE = $post;

			?>

		<?php endwhile; endif; ?>

	</article>

<?php get_footer(); ?>
