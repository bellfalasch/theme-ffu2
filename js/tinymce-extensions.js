(function() {
	tinymce.PluginManager.add('addTinyMCE_GamepadMenu_button', function( editor, url ) {
		editor.addButton( 'addTinyMCE_GamepadMenu_button', {
			title: 'Ikoner från spelkontroller',
			type: 'menubutton',
			icon: 'icon dashicons-welcome-comments',
			menu:  [
				{
					text: 'PlayStation (PSX)',
					value: '',
					onclick: function() {
						editor.insertContent(this.value());
					},
					menu: [
						{
							text: 'Kryss',
							value: '<i class="gamepad psx ex"></i>',
							icon: 'icon gamepad psx ex',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'Cirkel',
							value: '<i class="gamepad psx circle"></i>',
							icon: 'icon gamepad psx circle',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'Fyrkant',
							value: '<i class="gamepad psx square"></i>',
							icon: 'icon gamepad psx square',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'Trekant',
							value: '<i class="gamepad psx triangle"></i>',
							icon: 'icon gamepad psx triangle',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'R1',
							value: '<i class="gamepad psx r1"></i>',
							icon: 'icon gamepad psx r1',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'R2',
							value: '<i class="gamepad psx r2"></i>',
							icon: 'icon gamepad psx r2',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'L1',
							value: '<i class="gamepad psx l1"></i>',
							icon: 'icon gamepad psx l1',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'L2',
							value: '<i class="gamepad psx l2"></i>',
							icon: 'icon gamepad psx l2',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'Upp',
							value: '<i class="gamepad psx up"></i>',
							icon: 'icon gamepad psx up',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'Höger',
							value: '<i class="gamepad psx right"></i>',
							icon: 'icon gamepad psx right',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'Ner',
							value: '<i class="gamepad psx down"></i>',
							icon: 'icon gamepad psx down',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'Vänster',
							value: '<i class="gamepad psx left"></i>',
							icon: 'icon gamepad psx left',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'Start',
							value: '<i class="gamepad psx start"></i>',
							icon: 'icon gamepad psx start',
							onclick: function() {
								editor.insertContent(this.value());
							}
						},
						{
							text: 'Select',
							value: '<i class="gamepad psx select"></i>',
							icon: 'icon gamepad psx select',
							onclick: function() {
								editor.insertContent(this.value());
							}
						}
					]
				}
		   ]
		});
	});
})();
