$().ready(function() {

	// ***************
	// Filter-funktion - skriv för att filtrera, med möjlighet att nollställa.
	// Check if we're on the filter template and add JS filter function
	// Inspired by: http://stackoverflow.com/questions/1604639/jquery-trying-to-filter-divs-on-keyup
	// ***************
	if ( $('body.page-template-page-filter').length ) {
		var body = $('body.page-template-page-filter');

		$(".filter-text").keyup( function() {
			var filter = $(this).val();
			var total = $(".filter-wrapper h2").length;
			var count = 0;

			if (filter != '') {
				$(".filter-clean").addClass('active');
				$(".filter-wrapper h2").each(function () {
					var parent = $(this).parent(),
						length = $(this).text().length > 0,
						id = parent.attr('id');

					if ( length && $(this).text().search(new RegExp(filter, "i")) < 0) {
						parent.hide();
						$('nav.submenu.simple a[href*="#' + id + '"]').parent().hide(); // a[href*=#]:not([href=#])
						//console.log(id);
					} else {
						parent.show();
						$('nav.submenu.simple a[href*="#' + id + '"]').parent().show();
						count++;
					}
				});
				if ( $('.filter-information-text').length ) {
					$('.filter-information-text').remove();
				}
				$('<p class="filter-information-text">Visar <strong>' + count + '</strong> träff av totalt ' + total + '.</p>').insertAfter($(".filter-box"));
			} else {
				if ( $('.filter-information-text').length ) {
					$('.filter-information-text').remove();
				}
				$(".filter-clean").removeClass('active');
			}
			//$("#filter-count").text(count);
		});

		// Hantera rensa/nollställ-knappen.
		$(".filter-clean").click( function() {
			$(".filter-wrapper").each(function () {
				$(this).show();
				$(".filter-text").val("");
				$("nav.submenu.simple li").show();
				$(".filter-clean").removeClass('active');
				$('.filter-information-text').remove();
			});
		});

		/*
		var formloc = body.find("h2:first-child"); // Find the first h2, insert the form there
		formloc.prepend('<div class="form-well"><form method="get" action=""><input type="text" name="filter" class="filter-search" /><input type="submit" value="Filtrera" /></form></div>');

		var i = 0;
		body.find("h2").each( function() {
			if ( i > 0) {
				$(this).prepend('</div>');
			}
			$(this).prepend('<div class="block-wrapper" style="overflow:hidden; height:35px;">');
			i++;
		});
		body += "</div>";
		*/
	}

	// ***************
	// When clicking anchors, smooth scroll to the destination on a page.
	// ***************
	// Smooth scrolling, from css-tricks - https://css-tricks.com/snippets/jquery/smooth-scrolling/
	$('a[href*=#]:not([href=#]):not(.accordian)').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[id=' + this.hash.slice(1) +']');

			// Get height of WP admin bar + FFU guide navbar, if there. Set as global (for smooth scrolling).
			var wpadmin = $("#wpadminbar").length ? $("#wpadminbar").height() : 0;
			var navbarHeight = $("#navbar").length ? $("#navbar").outerHeight() : 0;
			if ($(window).width() >= 950) {
				navbarHeight = 0;
			}

			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - navbarHeight - wpadmin
				}, 1000);
				return false;
			}
		}
	});

	// ***************
	// Accordian page template, navigation
	// ***************
	if ( $('body.page-template-page-accordian').length ) {

		$('article .filter-wrapper.accordian').hide(); // Hide all text
		$('article a.accordian').click( function() { // Click menu to ...

			var link = $(this);
			var id = link.attr("href"); // Fetch id from link href
			var elem = $(id);

			if (elem.length) {
				elem.insertAfter(link); // Move matching block up
				elem.children("h2:first-child").hide(); // Hide first h2 (since that is in the menu)
				elem.show(); // Show the text

				$('html,body').animate({
					scrollTop: link.offset().top - wpadmin
				}, 1000);
				return false;
			}

		});

	}

	// ***************
	// Wrap all tables on mobile (for overflow:scroll toggling)
	// ***************
	if ($(window).width() < 950) {
		// Wrap tables so we can side scroll them easier
		$( "article table" ).wrap( "<div class='table-wrapper'></div>" );

		// Sticky nav
		var stickyNavTop = $('#navbar').offset().top;
		var stickyNav = function() {
	    var scrollTop = $(window).scrollTop();
			if (scrollTop > stickyNavTop) {
				$('#navbar').addClass('sticky');
				$('body').addClass('menu-is-sticky');
			} else {
				$('#navbar').removeClass('sticky');
				$('body').removeClass('menu-is-sticky');
			}
		};
		stickyNav();
					// and run it again every time you scroll
		$(window).scroll(function() {
			stickyNav();
		});
	}

	// ***************
	// Mobile main site menu (plays together with stickyness)

	// Opening menu:
	$(".menu-toggler.toggle-open").click(function () {
		var navbar = $("#navbar");
		navbar.addClass('open');
		if (navbar.hasClass('sticky')) {
			navbar.removeClass('sticky');
			navbar.addClass('was-sticky');
		}
	});
	// Closing menu:
	$(".menu-toggler.toggle-close").click(function () {
		var navbar = $("#navbar");
		navbar.removeClass('open');
		if (navbar.hasClass('was-sticky')) {
			navbar.addClass('sticky');
			navbar.removeClass('was-sticky');
		}
	});


	// **************
	// | Huvudmenyn |
	// **************
	// Stäng alla menyer (som startar som helt öppna).
	$(".ff_menu li:not(.in-path, .is-active) .sub-menu").addClass('closed');

	// Sätt dit FF-hand på aktiv menypunkt.
	var img = $('<span id="ffmenu_active_hand">');
	$('.ff_menu .is-active').before(img);

	// Footerns menyer
	//$("#page_expander li:not(.in-path, .is-active) .sub-menu").addClass('closed');
	//$("#page_expander .haskids:not(.in-path, .is-active) .sub-menu").hide();

	$(".ff_menu .menu > li > a").click( function() {
		var el = $(this);
		var sub = el.parent('li').children(".sub-menu");
		// If this menu item contains a sub-menu, which is closed, open it!
		if ( sub && sub.hasClass('closed') ) {
			sub.removeClass('closed');
			return false;
		} else {
			return true; // Tillåt navigering till huvudsidan efter meny öppnats.
		}
	});
	// TODO: Try arrow-clicking to close a meny
	/*
	$(".sub-menu:not(.closed)::before").click( function() {
		$(this).addClass('closed');
	});
	*/

});
