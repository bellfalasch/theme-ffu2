(function() {
   tinymce.PluginManager.add('addTinyMCE_shortcodes_button', function( editor, url ) {
     editor.addButton('addTinyMCE_shortcodes_button', {
        text: 'Shortcodes',
        icon: false,
        type: 'menubutton',
        menu: [
              {
               text: 'Liten boss-ruta',
               onclick: function() {
                  editor.insertContent('[boss url="ÄNDRA"]');
                         }
               },
              {
               text: 'Stor boss-ruta',
               onclick: function() {
                  editor.insertContent('[bigboss slug="ÄNDRA"]Valfri text[/bigboss]');
                }
              }
        ]
     });
   });
})();
