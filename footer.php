<!-- footer --><?php

	global $PAGE;

	if (isset($PAGE)) {
		edit_post_link('Öppna i Wordpress', '<p class="adminlink">', '</p>', $PAGE->ID);
	}

?>
			</div><!-- #content -->

		</div><!-- #content_wrapper -->

		<?php require_once( get_template_directory() . '/inc/init-lightcase-js.php' ); ?>

		<div id="before_footer">
			<div class="sidepusher"></div>
			<div class="content">
				<div class="pageinfo">
					<?php
						// The new way to output footer data
						if ( isset($PAGE) ) {

							$created = $PAGE->post_date;
							$edited = $PAGE->post_modified;
							$editor = str_replace( array("tigerlaugh","Admin"),"Webmaster", $PAGE->post_author );

							$edited = strtotime($edited);
							$created = strtotime($created);

							$edited = "" . date('H:i',$edited) . ", <strong>" . strtolower(strDateToSwedish('D',$edited)) . "</strong> den <strong>" . date('j',$edited) . " " . strtolower(strDateToSwedish('M',$edited)) . "</strong>, " . date('Y',$edited);
							$created = "den <strong>" . date('j',$created) . " " . strtolower(strDateToSwedish('M',$created)) . "</strong>, " . date('Y',$created);

							echo "Senast ändrad " . $edited . "<br />";
							//echo "X Ansvarig utgivare är <strong>" . $editor . "</strong><br />"; // TODO: Get name here instead of ID
							echo "(Sidan är skapad " . $created . ")";
						}
					?>
				</div>
				<div class="jags">
					<p class="theme-produced-by">
						Theme <strong>FFU2 v1.7</strong> är designat och utvecklat av
						<a href="http://www.westbergwebbproduktion.no/?utm_source=endnav-guide&amp;utm_medium=produced_by&amp;utm_campaign=FFU2.5">W2 Studio</a>
					</p>
				</div>
			</div>
		</div>
	</div><!-- #main_wrapper -->

	<div class="footer-bonus-area">
		<div class="wrapper">

			<div class="leftside">
				<h5>Sidkarta:</h5>
			</div>

			<div class="wrapper">
				<footer>
					<nav role="navigation">
					<?php
						// This menu comes from /sidebar.php and /inc/menu.php, we're just reusing that premade HTML ... nice!
						// Do some manipulations on the html before output.
						global $menuHtml;
						$menuHtml = str_replace('<h4>','<div class="box"><h4>',$menuHtml);
						$menuHtml = str_replace('<a href=','<a rel="nofollow" href=',$menuHtml);
						$menuHtml = str_replace('<div class="ff_menu">','',$menuHtml);
						echo $menuHtml;
					?>
					</nav>
				</footer>
			</div>

		</div>
	</div><!-- .footer-bonus-area -->

	<?php wp_footer(); ?>

    <?php do_action('ffu_endnav'); ?>
	<?php do_action('body_closed'); ?>

</body>
</html>
