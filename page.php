<?php
/*
	Template Name: Vanlig sida (medium/normal bild)
*/
?><?php get_header(); ?>
<?php get_sidebar(); ?>

	<article>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<h1><?php the_title(); ?></h1>

			<?php
				// If Thumbnail ("Featured Image") is added, display it
				if ( has_post_thumbnail() ) {
					echo "<div class=\"alignright\">" . get_the_post_thumbnail($id, 'medium') . "</div>";
				}

				// Allow for page stumps to automatically output info how to apply for a job =)
				if ( mb_strlen(get_the_content()) < 4) {
					emptyPagePlaceholder();
				} else {
					the_content();
				}

				// Needed for the footer.php to being able to fetch dates and author info from current page!
				global $PAGE;
				$PAGE = $post;
			?>

		<?php endwhile; endif; ?>

	</article>

<?php get_footer(); ?>
