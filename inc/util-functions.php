<?php

	function strDateToSwedish($what, $thedate)
	{
		// From day number set day name
		$daynum = date("N", $thedate );
		switch($daynum) {
			case 1:	 $day = "Måndag";  break;
			case 2:	 $day = "Tisdag";  break;
			case 3:	 $day = "Onsdag";  break;
			case 4:	 $day = "Torsdag"; break;
			case 5:	 $day = "Fredag";  break;
			case 6:	 $day = "Lördag";  break;
			case 7:	 $day = "Söndag";  break;
			default: $day = "Okänt";   break;
		}

		// From month number set month name
		$monthnum  = date("n", $thedate );
		switch($monthnum) {
			case 1:  $month = "Januari";   break;
			case 2:  $month = "Februari";  break;
			case 3:  $month = "Mars";      break;
			case 4:  $month = "April";     break;
			case 5:  $month = "Maj";       break;
			case 6:  $month = "Juni";      break;
			case 7:  $month = "Juli";      break;
			case 8:  $month = "Augusti";   break;
			case 9:  $month = "September"; break;
			case 10: $month = "Oktober";   break;
			case 11: $month = "November";  break;
			case 12: $month = "December";  break;
			default: $month = "Okänt";     break;
		}

		// Depending on what we wanted to get from the function return that
		$returnthis = '';
		switch( strtoupper($what) )
		{
			case 'M': $returnthis = $month;	break;
			case 'D': $returnthis = $day;	break;
		}
		return htmlentities($returnthis, ENT_QUOTES, "UTF-8");
	}

	function titleToUrl($url) {
		$url = trim( mb_strtolower($url, "UTF-8") ); // UTF-8 forced or Ajax won't work
		$url = str_replace('å', 'a', $url); // Replace Nordic letters
		$url = str_replace('ä', 'a', $url);
		$url = str_replace('æ', 'a', $url);
		$url = str_replace('ö', 'o', $url);
		$url = str_replace('ø', 'o', $url);
		$url = str_replace('–', '', $url); // WP styled dash
		$url = str_replace('–', '', $url); // WP styled dash, v2
		$url = str_replace(' ', '-', $url); // Space to dash
		$url = str_replace(',', '', $url); // Everything else removed
		$url = str_replace('.', '', $url);
		$url = str_replace(':', '', $url);
		$url = str_replace('&#8220;', '“', $url); // WP quotes
		$url = str_replace('&#8221;', '”', $url); // WP quotes
		$url = str_replace('&#8211;', '-', $url); // WP dash
		$url = str_replace('&lt;', '<', $url);
		$url = str_replace('&gt;', '>', $url);
		$url = str_replace('&#8217;', '’', $url); // WP apostrophe
		$url = str_replace('---', '-', $url); // Triple space to one dash
		$url = str_replace('--', '-', $url); // Double space to one dash
		$url = str_replace('<', '', $url);
		$url = str_replace('>', '', $url);
		$url = str_replace(';', '', $url);
		$url = str_replace('!', '', $url);
		$url = str_replace('?', '', $url);
		$url = str_replace('&', '', $url);
		$url = str_replace('%', '', $url);
		$url = str_replace('#', '', $url);
		$url = str_replace('=', '', $url);
		$url = str_replace('\'', '', $url);
		$url = str_replace('/', '', $url);
		$url = str_replace('(', '', $url);
		$url = str_replace(')', '', $url);
		$url = str_replace('{', '', $url);
		$url = str_replace('}', '', $url);
		$url = str_replace('[', '', $url);
		$url = str_replace(']', '', $url);
		$url = str_replace('"', '', $url);
		$url = str_replace('“', '', $url);
		$url = str_replace('”', '', $url);
		$url = str_replace("'", '', $url); // Apostrophe
		$url = str_replace("’", '', $url); // Apostrophe
		$url = urlencode( $url );
		return $url;
	}

?>
