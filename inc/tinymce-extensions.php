<?php

// Create my own button with a menu inside TinyMCE
////////////////////////////////////////////////////////////////////////
function addTinyMCE_GamepadMenu() {
  global $typenow;
  // check user permissions
  if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) {
  return;
  }
  // verify the post type
  if( ! in_array( $typenow, array( 'post', 'page' ) ) )
    return;
  // check if WYSIWYG is enabled
  if ( get_user_option('rich_editing') == 'true') {
    add_filter("mce_external_plugins", "addTinyMCE_GamepadMenu_addPlugin");
    add_filter('mce_buttons_2', 'addTinyMCE_GamepadMenu_registerButton');
  }
}
add_action('admin_head', 'addTinyMCE_GamepadMenu');
function addTinyMCE_GamepadMenu_addPlugin($plugin_array) {
  $plugin_array['addTinyMCE_GamepadMenu_button'] = get_bloginfo('template_directory') . "/js/tinymce-extensions.js";
  return $plugin_array;
}
function addTinyMCE_GamepadMenu_registerButton($buttons) {
  array_push($buttons, "addTinyMCE_GamepadMenu_button");
  return $buttons;
}

function addTinyMCE_GamepadMenu_registerCss() {
  wp_enqueue_style('addTinyMCE_GamepadMenu', get_bloginfo('template_directory') . "/css/tinymce-extensions.css" );
}
add_action('admin_enqueue_scripts', 'addTinyMCE_GamepadMenu_registerCss');


// Create a button, with dropdown, in TinyMCE, for inserting shortcodes
////////////////////////////////////////////////////////////////////////
// hooks your functions into the correct filters
function addTinyMCE_shortcodes_button() {
  // check user permissions
  if ( !current_user_can( 'edit_posts' ) &&  !current_user_can( 'edit_pages' ) ) {
    return;
  }
  // check if WYSIWYG is enabled
  if ( 'true' == get_user_option( 'rich_editing' ) ) {
    add_filter( 'mce_external_plugins', 'addTinyMCE_shortcodes_addPlugin' );
    add_filter( 'mce_buttons', 'addTinyMCE_shortcodes_registerButton' );
  }
}
add_action('admin_head', 'addTinyMCE_shortcodes_button');

// register new button in the editor
function addTinyMCE_shortcodes_registerButton( $buttons ) {
  array_push( $buttons, 'addTinyMCE_shortcodes_button' );
  return $buttons;
}
// declare a script for the new button
// the script will insert the shortcode on the click event
function addTinyMCE_shortcodes_addPlugin( $plugin_array ) {
    $plugin_array['addTinyMCE_shortcodes_button'] = get_stylesheet_directory_uri() .'/js/tinymce-shortcodes.js';
    return $plugin_array;
}

// Add "style" settings to TinyMCE drop down
////////////////////////////////////////////////////////////////////////
add_filter( 'tiny_mce_before_init', 'my_mce_before_init' );
function my_mce_before_init( $settings ) {

  $style_formats = array(
    array(
      'title' => 'Sidebar info',
      'block' => 'div',
      'classes' => 'sidebar',
      'wrapper' => true
    ),
    array(
      'title' => 'Sidebar image',
      'block' => 'div',
      'classes' => 'sidebar img',
      'wrapper' => true
    ),
    array(
      'title' => 'Sidebar imagebox',
      'block' => 'div',
      'classes' => 'sidebar img_box',
      'wrapper' => true
    ),
    array(
      'title' => 'Informationsruta',
      'block' => 'div',
      'classes' => 'infobox',
      'wrapper' => true
    ),
    array(
      'title' => 'Svag text (extrainfo)',
      'block' => 'p',
      'classes' => 'weak-text',
      'wrapper' => false
    ),
    array(
      'title' => 'Smal informationsruta',
      'block' => 'div',
      'classes' => 'infobox light',
      'wrapper' => true
    ),
    array(
      'title' => 'Varningsruta',
      'block' => 'div',
      'classes' => 'infobox warning',
      'wrapper' => true
    ),
    array(
      'title' => 'Lyckat-ruta',
      'block' => 'div',
      'classes' => 'infobox success',
      'wrapper' => true
    ),
    array(
      'title' => 'Svag ruta',
      'block' => 'div',
      'classes' => 'infobox muted',
      'wrapper' => true
    ),
    array(
      'title' => 'Full sidbredd',
      'block' => 'div',
      'classes' => 'fullcover',
      'wrapper' => true
    ),
    array(
      'title' => 'Inline Quote',
      'inline' => 'q',
      'classes' => 'inline',
      'wrapper' => false
    ),
    array(
      'title' => 'Infobox v2 - Dont miss',
      'block' => 'div',
      'classes' => 'infobox2 dontmiss',
      'wrapper' => true
    ),
    array(
      'title' => 'Infobox v2 - Warning',
      'block' => 'div',
      'classes' => 'infobox2 warning',
      'wrapper' => true
    ),
    array(
      'title' => 'Infobox v2 - Did you know?',
      'block' => 'div',
      'classes' => 'infobox2 didyouknow',
      'wrapper' => true
    ),
    array(
      'title' => 'Infobox v2 - Help',
      'block' => 'div',
      'classes' => 'infobox2 help',
      'wrapper' => true
    ),
    array(
      'title' => 'Wide infobox v2!',
      'block' => 'div',
      'classes' => 'wide',
      'wrapper' => false
    )
  );

  $settings['style_formats'] = json_encode( $style_formats );
  $settings['block_formats'] = "Paragraf=p; Siderubrik=h2; Underrubrik=h3; Sidebar-rubrik (3)=h4; Infobox2-minirubrik=h6";
  return $settings;
}
?>
