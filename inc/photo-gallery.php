<?php

function ffu_photo_gallery_generator($atts) {

	global $post;
	$pid = $post->ID;
	$gallery = '<ol class="linkbox gallery">';

	if (empty($pid)) {$pid = $post['ID'];}

	if (!empty( $atts['ids'] ) ) {
	   	$atts['orderby'] = 'post__in';
	   	$atts['include'] = $atts['ids'];
	}

	extract(shortcode_atts(array('orderby' => 'menu_order ASC, ID ASC', 'include' => '', 'id' => $pid, 'itemtag' => 'dl', 'icontag' => 'dt', 'captiontag' => 'dd', 'columns' => 3, 'size' => 'large', 'link' => 'file'), $atts));
		
	$args = array('post_type' => 'attachment', 'post_status' => 'inherit', 'post_mime_type' => 'image', 'orderby' => $orderby);

	if (!empty($include)) {
        $args['include'] = $include;
    } else {
	   	$args['post_parent'] = $id;
		$args['numberposts'] = -1;
	}

	if ($args['include'] == "") {
        $args['orderby'] = 'date';
        $args['order'] = 'asc';
    }

	$images = get_posts($args);

	foreach ( $images as $image ) {
        $full_img_url = wp_get_attachment_image_url($image->ID, 'large');
		$thumb_img_url = wp_get_attachment_image_url($image->ID, 'thumbnail'); // Thumbnail is also default size returned
		$split_pos = strpos($full_img_url, 'wp-content');
		$split_len = (strlen($full_img_url) - $split_pos);
		$abs_img_url = substr($full_img_url, $split_pos, $split_len);
        $full_info = @getimagesize(ABSPATH.$abs_img_url);

		$alt_text = get_post_meta($image->ID, '_wp_attachment_image_alt', true);

		if ($image->post_excerpt != '') {
			$img_title = $image->post_excerpt;
		} elseif ($image->post_title != '') {
			$img_title = $image->post_title;
		}

		$gallery .= <<<EOT
			<li>
                <a href="{$full_img_url}" title="{$image->post_title}" data-rel="lightcase:group">
                    <img src="{$thumb_img_url}" alt="{$alt_text}" />
                    {$img_title}
                </a><br />
                <small>( {$full_info[0]} x {$full_info[1]} <abbr title="Pixlar">px</abbr> )</small>
			</li>
EOT;
	}
	
	return $gallery . '</ol>';
}

?>