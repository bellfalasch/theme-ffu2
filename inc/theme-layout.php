<?php

	function emptyPagePlaceholder() {
	?>
		<div class="infobox2 didyouknow">
			<h6>Hjälp oss</h6>
			<img class="alignright" style="width: 90px;" src="https://om.ffuniverse.nu/wp-content/uploads/2017/05/mog_big.png" alt="Pixelerad bild av Mog, från FFVI" />
			<h4>Skriv denna sida!</h4>
			<p>Visste du att FFUniverse är på jakt efter <strong>nya duktiga Final Fantasy-fans</strong> som har lust att fylla sidorna och guiderna med information? Det är bara att slå av en liten prat med oss på <a href="https://www.facebook.com/ffuniverse.nu">vår Facebook-sida</a>. Om du vill veta mer om att skriva på FFU så kan du kolla på <a href="https://om.ffuniverse.nu/hjalp-oss/">våra "hjälp oss"-sidor</a>.</p>
		</div>
		<p>Denna sida hade vi tänkt fylla med information, men vi har inte kommit så långt än, tyvärr. Kanske <strong>du</strong> kunde hjälpa oss? Läs mer i rutan här intill.</p>
	<?php
	}

	// Generate character portrait
	function echoCharacterPortrait($portrait, $thumbnail) {
		echo '<div class="portrait">';
		if ( !empty($portrait) ) {
			echo "<img src=\"" . $portrait["sizes"]["medium"] . "\" alt=\"\">";
		} else {
			echo $thumbnail;
		}
		echo '</div>';
	}

	// Generate message about none playable character
	function echoCharacterNotPlayable($spelbar,$gender) {
		if ( $spelbar === false && !is_null($spelbar) ) {

			if ( $gender == "Man" ) {
				$hen = "han";
			} elseif ( $gender == "Kvinna" ) {
				$hen = "hon";
			} else {
				$hen = "den";
			}
	?>
			<div class="infobox short">
				<p>
					* Detta är inte en spelbar karaktär, men <?= $hen ?> har en viktig del i storyn.
				</p>
			</div>
	<?php
		}
	}

	// Support-function to fetch a single/specific boss based on URL
	function getBoss($slug) {
		$page = get_posts([
				'name'      => $slug,
				'post_type' => 'page'
			]);
		return $page;
	}

	// [boss url="slug"]
	function get_bosslink( $atts ) {
		$a = shortcode_atts( array(
			'url' => '',
		), $atts );
		$theBoss = getBoss($a['url']);

		if ( $theBoss ) {
			$hp = get_field('hp', $theBoss[0]->ID);

			$content = '<div class="infobox boss"><strong>Boss:</strong> <a href="' . get_page_link( $theBoss[0]->ID ) . '" title="Läs hur du besegrar ' . $theBoss[0]->post_title . '">' . $theBoss[0]->post_title . '</a>';

			if ( $hp != "" ) { // Only add HP data if we have it stored
				$hp = number_format($hp, 0, ',', ' ');
				$content .= '<span class="hp">HP: ' . $hp . '</span>';
			}
			$content .= '</div>';
		}

		return $content;
	}
	add_shortcode( 'boss', 'get_bosslink' );

	// [bigboss slug="slug"]Fritext[/bigboss1]
	function get_bigbosslink($atts = [], $content = null) {
		// normalize attribute keys, lowercase
    	$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$a = shortcode_atts( array(
			'slug' => 'guard-scorpion', // default values
		), $atts );
		$theBoss = getBoss($a['slug']);
		$html = "";

		if ( $theBoss ) {

			$content = apply_filters('the_content', $content);
			$html = '<div class="fullcover boss"><h6>Bossmöte</h6><h3>' . $theBoss[0]->post_title . '</h3>';
			$html .= '<div class="infobox boss">';

			$hp = get_field('hp', $theBoss[0]->ID);
			if ( $hp != "" ) { // Only add HP data if we have it stored
				$hp = number_format($hp, 0, ',', ' ');
				$html .= '<strong>HP</strong> ' . $hp;
			}

			$weaknesses = get_field('svagheter', $theBoss[0]->ID);
			if ( $weaknesses != "" ) { // Only add HP data if we have it stored
				$html .= '<strong>Svagheter</strong> ' . $weaknesses;
			}

			$strenghts = get_field('stark_mot', $theBoss[0]->ID);
			if ( $strenghts != "" ) { // Only add HP data if we have it stored
				$html .= '<strong>Stark mot</strong> ' . $strenghts;
			}

			$html .= '</div>';
			$html .= '<p>' . $content .'</p>';
			$html .= '<p class="additional-information">Behöver du mer hjälp? <a href="' . get_page_link( $theBoss[0]->ID ) . '">Fler tips för att besegra ' . $theBoss[0]->post_title . ' &raquo;</a></p>';
			$html .= '</div>';
		}

		return $html;
	}
	add_shortcode( 'bigboss', 'get_bigbosslink' );


	// Render our breadcrumbs (pretty simple setup)
	// http://www.branded3.com/blogs/creating-a-really-simple-breadcrumb-function-for-pages-in-wordpress/
	function get_breadcrumb() {
		global $post;
		$i = 3;

		if ( !is_front_page() ) {
			$trail = '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="home"><a itemprop="item" href="' . get_bloginfo('url') . '" title="Gå till startsidan för ' . get_bloginfo('name') . '-guiden"><span itemprop="name">' . get_bloginfo('name') . '</span></a><meta itemprop="position" content="2" /></li>';

			if ($post->post_parent && !is_search()) {
				$parent_id = $post->post_parent;

				while ($parent_id) {
					$page = get_page($parent_id);
					$pageTitle = get_the_title($page->ID);
					if (is_attachment($page->ID)) { $pageTitle = 'Bildvisning'; }
					if (is_search($page->ID)) { $pageTitle = 'Sök'; }
					$breadcrumbs[] = '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a itemprop="item" href="' . get_permalink($page->ID) . '" title="' . $pageTitle . '"><span itemprop="name">' . $pageTitle . '</span></a><meta itemprop="position" content="' . $i . '" /></li>';
					$parent_id = $page->post_parent;
					$i++;
				}

				$breadcrumbs = array_reverse($breadcrumbs);
				foreach($breadcrumbs as $crumb) $trail .= $crumb;
			}

			// Add current page title (not clickable)
			$pageTitle = get_the_title($post->ID);
			if (is_search()) { $pageTitle = 'Sök'; }
			if (is_attachment()) { $pageTitle = 'Bildvisning'; }
			$trail .= '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="current_item"><span itemprop="name">' . $pageTitle . '</span><meta itemprop="item" content="' . get_permalink($post->ID) . '" /><meta itemprop="position" content="' . $i . '" /></li>';

		} else {
			// Frontpage
			$trail = '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="home current_item"><span itemprop="name">' . get_bloginfo('name') . '</span><meta itemprop="item" content="' . get_bloginfo('url') . '" /><meta itemprop="position" content="2" /></li>';
		}

		return $trail;
	}

	// Printa en länkbox genom att mata den med länk, title, och eventuell thumbnail
	function printLinkboxWithThumb($title, $link, $image, $thumb = false) {

		$thumbnail_or_link = printThumbOrPlaceholder($image);

		if ( !$thumb ) {
			$thumbnail_or_link = ""; // Reset the thumbnail/placeholder
		}

		echo "
			<li>
				<a href=\"" . $link . "\">
						" . $thumbnail_or_link . "
					" . $title . "
				</a>
			</li>";
	}

	// Send in värdet av get_post_thumbnail och få ut bilden, eller en placeholder-box (för .linkbox)
	function printThumbOrPlaceholder($image) {
		return ($image != '') ? $image : <<< EOT
<div class="placeholder"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 490.667 490.667" style="enable-background:new 0 0 490.667 490.667;" xml:space="preserve">
	<g>
		<path d="M309.333,128h-12.48c-4.416-12.416-16.277-21.333-30.187-21.333H224c-14.528,0-26.816,9.728-30.699,22.997
			c-1.813,1.152-3.285,2.859-4.139,4.928c-1.664,3.989-0.747,8.576,2.304,11.627l29.035,29.035
			c7.808-2.773,16.085-4.565,24.832-4.565c41.173,0,74.667,33.493,74.667,74.667c0,8.747-1.792,17.024-4.565,24.832l46.613,46.613
			c2.027,2.027,4.736,3.115,7.552,3.115c0.789,0,1.579-0.085,2.368-0.277c3.563-0.811,6.485-3.392,7.701-6.848
			c2.901-8.235,4.373-16.576,4.373-24.789v-85.333C384,161.493,350.507,128,309.333,128z"/>
		<path d="M245.333,0C110.059,0,0,110.059,0,245.333s110.059,245.333,245.333,245.333s245.333-110.059,245.333-245.333
			S380.608,0,245.333,0z M245.333,469.333c-123.52,0-224-100.48-224-224c0-57.92,22.293-110.613,58.496-150.421l52.267,52.267
			c-15.808,14.08-25.429,34.176-25.429,55.488V288c0,41.173,33.493,74.667,74.667,74.667h128c10.773,0,21.291-2.773,31.019-7.232
			l55.403,55.403C355.947,447.04,303.253,469.333,245.333,469.333z M170.667,245.333c0-16.725,5.931-31.851,15.296-44.288
			l103.68,103.68C277.205,314.091,262.059,320,245.333,320C204.16,320,170.667,286.507,170.667,245.333z M410.837,395.755
			L94.933,79.851c39.808-36.203,92.501-58.496,150.421-58.496c123.52,0,224,100.48,224,224
			C469.333,303.253,447.04,355.947,410.837,395.755z"/>
	</g>
</svg></div>
EOT;
	}

	function siblingmenu_html($prev_item,$next_item,$id,$parent,$title,$title_plural,$parent_link) {
		?>
			<nav class="sibling-navigator">
				<div class="block<?= (is_null($prev_item)) ? " empty" : ""; ?>">
					<?php if ( ! is_null($prev_item) ) { ?>
						<a href="<?= get_page_link( $prev_item->ID ) ?>" title="<?= $prev_item->post_title ?>" class="prev">
							<small>
								<strong>Föregående</strong>
								<?= $title ?>
							</small><br />
							<?= $prev_item->post_title ?>
						</a>
					<?php } else { ?>
						<p class="no-link">
							<small>
								<strong>Föregående</strong>
								<?= $title ?>
							</small><br />
							Du är på första
						</p>
					<?php } ?>
				</div>
				<div class="block home">
					<a href="<?= get_page_link( $parent ) ?>" title="<?php get_the_title( $parent ) ?>">
						<?= $parent_link ?>
					</a>
				</div>
				<div class="block<?= (is_null($next_item)) ? " empty" : ""; ?>">
					<?php if ( ! is_null($next_item) ) { ?>
						<a href="<?= get_page_link( $next_item->ID ) ?>" title="<?= $next_item->post_title ?>" class="next">
							<small>
								<strong>Nästa</strong>
								<?= $title ?>
							</small><br />
							<?= $next_item->post_title ?>
						</a>
					<?php } else { ?>
						<p class="no-link">
							<small>
								<strong>Nästa</strong>
								<?= $title ?>
							</small><br />
							Du är på sista
						</p>
					<?php } ?>
				</div>
			</nav>
		<?php
	}

	// Navigation menu for any sibling pages, with custom titles/verbs
	function get_sibling_menu( $parent, $id, $title, $title_plural ) {
		$args = array(
			'sort_order' => 'ASC',
			'sort_column' => 'menu_order',
			'hierarchical' => 0,
			'child_of' => 0,
			'parent' => $parent,
			'post_type' => 'page',
			'post_status' => 'publish'
		);
		$sibling_menu = get_pages($args);
		// var_dump( $sibling_menu );
		$menu_length = count($sibling_menu);
		$prev_item = null;
		$next_item = null;

		// Loop through all the items, locate current page ID
		for ( $i = 0; $i < $menu_length; $i++ ) {
			// If current page ID, store the prev. and next items!
			if ( $sibling_menu[$i] && $sibling_menu[$i]->ID == $id ) {
				if ( $i > 0 ) {
					$prev_item = $sibling_menu[$i-1];
				}
				if ( $i < $menu_length-1) {
					$next_item = $sibling_menu[$i+1];
				}
			}
		}

		siblingmenu_html($prev_item,$next_item,$id,$parent,$title,$title_plural,"Visa alla <strong>" . $title_plural . "</strong>");
	}

	// Navigation menus on the guide template
	function get_guide_menu( $parent, $id, $nofollow = false, $button = false ) {
		// http://codex.wordpress.org/Function_Reference/get_pages
		$args = array(
			'sort_order' => 'ASC',
			'sort_column' => 'menu_order',
			'hierarchical' => 0,
			'child_of' => 0,
			'parent' => $parent,
			'post_type' => 'page',
			'post_status' => 'publish'
		);
		$guide_menu = get_pages($args);
		// var_dump( $guide_menu );
		$menu_length = count($guide_menu);
		$prev_item = null;
		$next_item = null;

		// Loop through all the items, locate current page ID
		for ( $i = 0; $i <= $menu_length; $i++ ) {
			// If current page ID, store the prev. and next items!
			if ( $guide_menu[$i]->ID == $id ) {
				if ( $i > 0 ) {
					$prev_item = $guide_menu[$i-1];
				}
				if ( $i < $menu_length) {
					$next_item = $guide_menu[$i+1];
				}
			}
		}

		$rel = "";
		if ( $nofollow ) {
			$rel = " rel=\"nofollow\"";
		}

?>

<?php if ($button) { ?>
	<div class="center">
	<?php if ( ! is_null($next_item) ) { ?>
		<a href="<?= get_page_link( $next_item->ID ) ?>" title="<?= $next_item->post_title ?>"<?= $rel ?> class="button next">Nästa del</a>
	<?php } else { ?>
		<p class="the-end">Slut på guiden - tack för att du läste!</p>
	<?php } ?>
	</div>
<?php
		}

		siblingmenu_html($prev_item,$next_item,$id,$parent,'del',$title_plural,'Guidens startsida');
	}

?>
