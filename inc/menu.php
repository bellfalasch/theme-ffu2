<?php

// TODO: Det bästa vore om man bara skippar DRAFT-items mycket tidigare.
// Då kan man kolla i starten om draft så skapa meny OM title matchar ett pattern, annars skippar man den och slipper då polution av last-item etc.
// Alltså: Loopa igenom hela menyn, och ut av det bygg upp ett object med det som är färdigt och ska rendras, så tar sig en egen funktion av HTML-jobben och som då kan vara mer effektiv. Något som detta (vi menyerna som omslutande egna arrays, och under ha varje menuitem med title, url?, id, haskids, ischild, inpath, mer?...):
// Saker som ischild, haschild, firstchild etc, borde gå att kolla utan att ha variabler för det. Om man plötsligt stöter på en parentid > 0 så är detta firstchild och kunde man då lägga open/close-grafiken in på denna ul så skulle man kunna lättare generera html för detta (då utan class på den innan).
// Flytta ut denna logik i en annan fil. Ha kall härifrån, men de ska gå till en egen menu.php-fil, gör det hela mer ryddigt.
// Blir en stor jobb, kan inte göras utan egen lokal dev-server.

// TODO: Också rensa upp variabler, förvirrande namn. Rörigt alltihop.

	/*
		** Concept **
		Block: de grupper som länkarna placeras in i.
		Item: varje enskild länk i menyn, oavsett "nivå".
		Menu: hela menyn.
		Submenu: ett uppsätt med länkar som underordnas en annan länk.

		Alltså: menyn består av ett eller flera block, med ett eller flera items med eller utan submenyer.
	*/
	$startpageLink = "<a href=\"" . get_site_url() . "\">Startsidan</a>";

	function getHtml_BlockInit($title) {
		return "<h4>" . $title . "</h4>
		<div class=\"ff_menu\">
			<ul class=\"menu\">";
	}

	// Fetch the menu tree data:
	function fetchMenu() {
		// http://codex.wordpress.org/Function_Reference/get_pages
		$args = array(
			'sort_order' => 'ASC',
			'sort_column' => 'menu_order',
			'hierarchical' => 1,
			'exclude' => '',
			'include' => '',
			'meta_key' => '',
			'meta_value' => '',
			'authors' => '',
			'child_of' => 0,
			'parent' => -1,
			'exclude_tree' => '',
			'number' => '',
			'offset' => 0,
			'post_type' => 'page',
			'post_status' => 'publish,draft'
		);
		return get_pages($args);
		// var_dump( $menuX[0] );
	}

	// PSEUDO: Tror det bästa är att ha en flat array och så specialla instruktioner i datan som ligger där, typ isHeader eller nåt annat liknande. Då kan render-funktionen vara superkorkad och endast spotta ut HTML genom att se på förra item, detta item, och nästa item och som mest jämföra dem för att se om och när någon tag ska stängas.
	function buildMenu() {
		$menuDataRaw = fetchMenu();
		$menuDataPrepared = [];
		$theItem = [];

		if (empty($menuDataRaw) || (mb_substr( $menuDataRaw[0]->post_title, 0, 3) != '===')) {
			// Create data for empty menu, or when block menu item is not the first item.
			$menuDataPrepared = [
				0 => [
					'text' => 'Guider',
					'isMenuBlock' => true
				],
				1 => [
					'text' => 'Startsidan',
					'url' => get_site_url(),
					'ID' => '0'
				]
			];
		} else {

			$i = 0;
			foreach($menuDataRaw as $item) {
				$theItem = [];
				if ($item->post_status == 'draft') {
					$firstChars = mb_substr( $item->post_title, 0, 3); // Extract first three characters, they contain a code on drafts that needs special handling. I know, hacky, but makes building FFU menus way easier for editors.

					// New block!
					if ($firstChars == '===') {
						$theItem = [
							'text' => mb_substr( $item->post_title, 3),
							'isMenuBlock' => true,
							'ID' => '-1'
						];
						if ($i < 1) {
							array_push($menuDataPrepared, $theItem);
							// Initiera meny, alla ska ha en startsida:
							$theItem = [
								'text' => 'Startsidan',
								'url' => get_site_url(),
								'ID' => '0'
							];
						}

					}
					// Links!
					if ($firstChars == "---") {
						$theItem = [
							'text' => mb_substr( $item->post_title, 3),
							'url' => wp_strip_all_tags( $item->post_content, true ),
							'isChild' => ($item->post_parent > 0),
							'ID' => '-1'
						];
					}
				} else {
					$theItem = [
						'text' => $item->post_title,
						'url' => get_page_link( $item->ID ),
						'isChild' => ($item->post_parent > 0),
						'post_parent' => $item->post_parent,
						'ID' => $item->ID
					];
				}
				if (!empty($theItem)) {
					array_push($menuDataPrepared, $theItem);
				}
				$i++;
			}
		}

		return $menuDataPrepared;
	}

	function renderMenu($menuData, $current) {
		// This should only add proper html to the menu and return it.
		// Needs to be able to store the output in a way so footer also can use it.
		// Allthough echo-ing is supposed to be faster, we might need to drop it for now.

		// TODO: Viktigt att footern helst har identisk html som sidebar. Just nu är det flera skillnader, och det är rätt irriterande för då är det svårare att skriva funktionarna för detta. Just nu skrivs egna headers/wrappers för varje meny för footern. Kan detta undvikas så kan vi återanvända detta bättre.
		// Dock läggs en "nofollow" på footerns länkar, det kan vi dock få till framöver genom en search and replace ... typ att standard är att lägga på den, men för sidebar menyn så strippas de bort (tror den vägen är lättare att hantera än omvänt även om det känns bakofram).

		$theHtml = "<div class=\"menu\">";
		$arraySize = count($menuData);

		for ($i=0; $i < $arraySize; $i++) {
			$dontCloseLI = true;
			$class = "";
			$theItem = $menuData[$i];
			$prevItem = ($i > 0) ? $menuData[$i-1] : null;
			$nextItem = ($i+1 < $arraySize) ? $menuData[$i+1] : null;
			if (!empty($theItem)) {
				$isMenuBlock = (isset($theItem['isMenuBlock']) ? $theItem['isMenuBlock'] : false);
				$theItemText = (isset($theItem['text']) ? $theItem['text'] : null);
				$prevItemIsChild = (isset($prevItem['isChild']) ? $prevItem['isChild'] : false);
				$theItemIsChild = (isset($theItem['isChild']) ? $theItem['isChild'] : false);
				$theItemID = (isset($theItem['ID']) ? $theItem['ID'] : 0);
				if ($isMenuBlock) {
					$dontCloseLI = true;
					// Don't close prev block if we're just beginning to render the menu.
					if ($i > 0) {
						$theHtml .= "</li></ul></div>";
					}
					if ($prevItemIsChild) {
						$theHtml .= "</li></ul>";
					}
					$theHtml .= getHtml_BlockInit($theItemText);
				} else {
					// Only close <li> if we're not opening submenues
					if (!$dontCloseLI) {
						$theHtml .= "</li>";
					}
					// Close submenues that are finished.
					if (!$theItemIsChild && $prevItemIsChild) {
						$theHtml .= "</ul>";
					}
					// New submenu needs to be opened
					if ($theItemIsChild && !$prevItemIsChild) {
						$theHtml .= "<ul class=\"sub-menu\">";
						$dontCloseLI = true;
					} else {
						$dontCloseLI = false;
					}

					if ( $current->ID == $theItemID ) {
						$class = " is-active";
					}
					// Current page we're on is the child of this menu item:
					if ( $current->post_parent == $theItemID && $theItemID !== '0') {
						$class = " in-path";
					}
					if ( is_home() && $theItemID === '0' ) {
						$class = " is-active";
					}

					$theHtml .= "<li class=\"menu-item" . $class . "\"><a href=\"" . $theItem['url'] . "\">" . $theItemText . "</a>";
				}
			}
		}
		$theHtml .= "</ul></div>";
		//$theHtml .= "</div>";

		return $theHtml;
	}

	function getMenu($current) {
		$menuDataPrepared = buildMenu();
		$renderedMenu = renderMenu($menuDataPrepared, $current);
		return $renderedMenu;
	}

?>
