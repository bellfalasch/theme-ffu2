<?php
// Add data to the Updates-table (for aspx-sites and our RSS) when a new Post is created, or updated
////////////////////////////////////////////////////////////////////////
function cleanSimple( $str ) {
//		$str = str_replace("\"", "&quot;", $str);
  $str = str_replace("“", "&quot;", $str);
  $str = str_replace("”", "&quot;", $str);
  $str = str_replace("“", "&quot;", $str);
  $str = str_replace("”", "&quot;", $str);
  $str = str_replace("-", "-", $str);
  $str = str_replace("–", "-", $str);
  $str = str_replace("‒", "-", $str);
  $str = str_replace("—", "-", $str);
  $str = str_replace("…", "...", $str);
  return $str;
}

function insert_into_updates_db( $post_id ) {
  // This code is only used and needed on our guides.
  //if ($_SERVER[HTTP_HOST] !== 'guide.ffuniverse.nu') {
  //  return;
  //}

  // If this is just a revision, don't save!
  if ( wp_is_post_revision( $post_id ) ) {
    return;
  }

  // If this isn't a 'post' post, don't update it.
  if ( 'post' != $_POST['post_type'] ) {
    return;
  }

  // Only save posts in the category "Uppdatering"
  if ( ! in_category("uppdatering", $post_id) ) {
    return;
  }

  // Only insert if we're publishing the post
  if ( 'publish' != get_post_status($post_id) ) {
    return;
  }

  // Settings
  global $wpdb;
  $start_date = "2015-03-02 00:00:01"; // The date we started with these kind of injections
  $update = get_post( $post_id );
  $post_title = cleanSimple( $update->post_title );
  $post_url = get_page_link( $update->ID );
  $post_body = cleanSimple( $update->post_content );
  $post_date = $update->post_date;

  // Name of the database table to use
  $table_name = $wpdb->base_prefix . 'ffu_updates';

  // Find out which site we're working on
  $current_site = get_site_url();

  // Remove trailing slashes, would ruin for us if kept
  if ( substr($current_site, -1) === "/" ) {
    $current_site = rtrim($current_site, '/');
  }

  // Find folder
  $folder = substr($current_site, strrpos($current_site, '/') + 1);

  // Map folder name to the ID in our updates database
  $site_id = 0;
  switch( $folder ) {
    case "ff1" : $site_id = 3; break;
    case "ff2" : $site_id = 4; break;
    case "ff3" : $site_id = 27; break;
    case "ff4" : $site_id = 5; break;
    case "ff5" : $site_id = 6; break;
    case "ff6" : $site_id = 7; break;
    case "ff7" : $site_id = 8; break;
    case "ff8" : $site_id = 9; break;
    case "ff9" : $site_id = 10; break;
    case "ff10" : $site_id = 11; break;
    case "ff11" : $site_id = 12; break;
    case "ff12" : $site_id = 13; break;
    case "ff13" : $site_id = 25; break;
    case "ff15" : $site_id = 28; break;
    case "ff10-2" : $site_id = 14; break;
    case "bravely-default" : $site_id = 26; break;
    case "kingdom-hearts" : $site_id = 17; break;
    case "chrono-trigger" : $site_id = 19; break;
    case "ff-tactics-advance" : $site_id = 15; break;
    case "ff-crystal-chronicles" : $site_id = 22; break;
    //case "sword-of-mana" : $site_id = 18; break;
    case "ff7-remake" : $site_id = 29; break;
    case "brave-fencer-musashi" : $site_id = 23; break;
  }

  // Connect to FFU main database for updates
  //$wpdbtest_otherdb = new wpdb(DB_USER, DB_PASSWORD, $db_database, DB_HOST);
  //$wpdbtest_otherdb->show_errors();

  // Check if the current item being posted already exists
  $item_exists_sql = "
      SELECT `id`, `published`
      FROM `" . $table_name . "`
      WHERE `site` = " . $site_id . "
      AND `wp_postid` = '" . $post_id . "'
      LIMIT 0, 1
  ;";

  $ourupdates = $wpdb->get_row( $item_exists_sql );

  // Did we find any data from before?
  if ( isset($ourupdates) && $ourupdates != null ) {

    $wpdb->query("
          UPDATE `" . $table_name . "`
          SET `headline` = '$post_title',
          `published` = '$post_date',
          `info` = '$post_body',
          `wp_fullurl` = '$post_url'
          WHERE `id` = " . $ourupdates->id . "
          AND `site` = $site_id
          ;"
    );

  } else {

    //echo "Didn't exists";    // Only run this date/title check if this is a post before we started hooking on ID's
    $doinsert = true;

    // If this is an update for an old post, check if its title exists (or it's the same date)
    if ( $start_date > $post_date ) {

      $doinsert = false;

      $item_exists_sql2 = "
          SELECT `id`, `published`
          FROM `" . $table_name . "`
          WHERE
            `wp_postid` = 0
            AND
            `site` = " . $site_id . "
            AND
            (
              `headline` LIKE '" . $post_title . "'
              OR
              LEFT(published,10) = '" . mb_substr($post_date,0,10) . "'
            )
          LIMIT 1;";

      $ourupdates2 = $wpdb->get_row( $item_exists_sql2 );

      // So, we didn't find it on ID, try and find it on title and or date
      if ( isset($ourupdates2) && $ourupdates2 != null ) {

        $wpdb->query("
              UPDATE `" . $table_name . "`
              SET `headline` = '$post_title',
              `published` = '$post_date',
              `info` = '$post_body',
              `wp_fullurl` = '$post_url',
              `wp_postid` = '$post_id'
              WHERE `id` = " . $ourupdates2->id . "
              AND `site` = $site_id
              ;"
        );

      }

    }

    if ( $doinsert ) {
      $item_insert_sql = "INSERT INTO `" . $table_name . "`(`site`,`headline`,`published`,`info`,`page`,`ffu`,`rss`,`wp_fullurl`,`wp_postid`)
                VALUES($site_id, '$post_title', '$post_date', '$post_body', NULL, 1, 1, '" . $post_url . "', '" . $post_id . "');
                ";

      $wpdb->query( $item_insert_sql );
    }

  }
}
add_action( 'save_post', 'insert_into_updates_db' );
?>
