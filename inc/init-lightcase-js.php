<script src="<?php bloginfo("template_url"); ?>/js/jquery.events.touch.min.js"></script>
<script src="<?php bloginfo("template_url"); ?>/js/lightcase.js"></script>
<script>
  $().ready(function() {
    $('a[data-rel^=lightcase]').lightcase({
      maxWidth : 960,
      maxHeight : 750,
      transition: 'scrollHorizontal',
      transitionOpen: 'elastic',
      labels: {
        'sequenceInfo.of': ' av '
      },
      swipe: true
    });
  });
</script>
