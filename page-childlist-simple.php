<?php
/*
	Template Name: Lista undersidor - enkel
*/
?><?php get_header(); ?>
<?php get_sidebar(); ?>

	<article>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<h1><?php the_title(); ?></h1>

			<?php
				// If Thumbnail ("Featured Image") is added, display it
				if ( has_post_thumbnail() ) {
					echo "<div class=\"alignright\">" . get_the_post_thumbnail($id, 'medium') . "</div>";
				}

				// Allow for page stumps to automatically output info how to apply for a job =)
				if ( mb_strlen(get_the_content()) < 4) {
					emptyPagePlaceholder();
				} else {
					the_content();
				}
			?>

			<?php

				$mypages = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'menu_order' ) );

				echo "<nav role=\"navigation\" class=\"submenu simple\"><ol>";
				foreach( $mypages as $page ) {
//					$content = $page->post_content;
//					if ( ! $content ) // Check for empty page
//						continue;

					echo "<li><a href=\"" . get_page_link( $page->ID ) . "\">" . $page->post_title . "</a></li>";

				}
				echo "</ol></nav>";
			?>

			<?php
				global $PAGE; // Needed for the footer.php to being able to fetch dates and author info from current page!
				$PAGE = $post;
			?>
		<?php endwhile; endif; ?>

	</article>

<?php get_footer(); ?>
