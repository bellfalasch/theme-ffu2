<?php
/*
	Template Name: Guide - startsida
*/
?><?php get_header(); ?>
<?php get_sidebar(); ?>

	<article>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<h1><?php the_title(); ?></h1>

			<?php
				// If Thumbnail ("Featured Image") is added, display it
				if ( has_post_thumbnail() ) {
					echo "<div class=\"alignright\">" . get_the_post_thumbnail($id, 'medium') . "</div>";
				}

				// Allow for page stumps to automatically output info how to apply for a job =)
				if ( mb_strlen(get_the_content()) < 4) {
					emptyPagePlaceholder();
				} else {
					the_content();
				}
			?>

<?php
	// check if the repeater field has rows of data
	if( have_rows('kapitel') ):
?>
	<div class="guide">
<?php
	 	// loop through the rows of data
    while ( have_rows('kapitel') ) : the_row();
?>
			<a class="chapter" href="<?php the_sub_field('lank'); ?>">
					<header>
						<?php
							$thumb = get_sub_field('bild');
							if ($thumb) {
						?>
						<?php echo wp_get_attachment_image($thumb, 'medium'); ?>
					<?php } ?>
					</header>
					<h3>
							<strong><?php the_sub_field('huvudtitel'); ?></strong>
							<?php the_sub_field('undertitel'); ?>
					</h3>
					<?php
					if (get_sub_field('innehall')) {
						$items = explode(",", get_sub_field('innehall'));
					?>
					<ol>
						<?php foreach ($items as $item) { ?>
							<li><?php echo trim($item); ?></li>
						<?php } ?>
					</ol>
					<?php
						}
					?>
			</a>
<?php
	    endwhile;
?>
	</div>
<?php
	else :
	    // no rows found
	endif;
?>

			<?php

				global $PAGE; // Needed for the footer.php to being able to fetch dates and author info from current page!
				$PAGE = $post;

			?>

		<?php endwhile; endif; ?>

	</article>

<?php get_footer(); ?>
