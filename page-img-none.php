<?php
/*
	Template Name: Vanlig sida (ingen bild)
*/
?><?php get_header(); ?>
<?php get_sidebar(); ?>

	<article>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<h1><?php the_title(); ?></h1>

			<?php the_content(); ?>

			<?php

				global $PAGE; // Needed for the footer.php to being able to fetch dates and author info from current page!
				$PAGE = $post;

			?>

		<?php endwhile; endif; ?>

	</article>

<?php get_footer(); ?>
